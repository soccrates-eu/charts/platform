# [0.5.0](https://gitlab.com/soccrates-eu/helm-chart/compare/0.4.4...0.5.0) (2022-06-28)


### Bug Fixes

* adds kibana ingressroute ([8514b62](https://gitlab.com/soccrates-eu/helm-chart/commit/8514b62c199b6922eab88f2a7b99a83df57978de))
* helm dependency update ([04ca43a](https://gitlab.com/soccrates-eu/helm-chart/commit/04ca43a2248892f1d9639a8d0b15aa25a56f5fef))
* kibana ingress service port ([97fdb27](https://gitlab.com/soccrates-eu/helm-chart/commit/97fdb275705745c56d96a4ea3975a9213682167c))


### Features

* adds kibana and elastic ([53d8fbd](https://gitlab.com/soccrates-eu/helm-chart/commit/53d8fbd568a6bc3c402724ad7fb1409d9811e470))

## [0.4.4](https://gitlab.com/soccrates-eu/helm-chart/compare/0.4.3...0.4.4) (2022-06-28)


### Bug Fixes

* disables kafka and kowl by default ([d29d59f](https://gitlab.com/soccrates-eu/helm-chart/commit/d29d59f39de49a918087d0bbbeafb6feb5b8de7d))

## [0.4.3](https://gitlab.com/soccrates-eu/helm-chart/compare/0.4.2...0.4.3) (2022-06-28)


### Bug Fixes

* adds missing middlewares to ingress routes ([db34abd](https://gitlab.com/soccrates-eu/helm-chart/commit/db34abd0e468c5940a98255bb315660f161b1a22))

## [0.4.2](https://gitlab.com/soccrates-eu/helm-chart/compare/0.4.1...0.4.2) (2022-06-28)


### Bug Fixes

* let user disable kowl ([e595d5a](https://gitlab.com/soccrates-eu/helm-chart/commit/e595d5aa6c04bb2469daf3f08a312651ae1b84b0))

## [0.4.1](https://gitlab.com/soccrates-eu/helm-chart/compare/0.4.0...0.4.1) (2022-06-28)


### Bug Fixes

* adds ingress route for kowl ([9efa7b4](https://gitlab.com/soccrates-eu/helm-chart/commit/9efa7b45468b50530440b2e857b2dac575e770d0))

# [0.4.0](https://gitlab.com/soccrates-eu/helm-chart/compare/0.3.0...0.4.0) (2022-06-28)


### Features

* adds kowl to monitor kafka ([146f334](https://gitlab.com/soccrates-eu/helm-chart/commit/146f334c25b5d49bdda24eb4c5010a67baa06c56))

# [0.3.0](https://gitlab.com/soccrates-eu/helm-chart/compare/0.2.0...0.3.0) (2022-06-28)


### Features

* adds kafka ([754b107](https://gitlab.com/soccrates-eu/helm-chart/commit/754b1077d4391af76e5341c5ef5692605825baf1))

# [0.2.0](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.27...0.2.0) (2022-06-27)


### Features

* adds minio ([b7fe99c](https://gitlab.com/soccrates-eu/helm-chart/commit/b7fe99c0c8c8df903afdc39323488c68374af081))

## [0.1.27](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.26...0.1.27) (2022-06-27)


### Bug Fixes

* default values ([bc5f954](https://gitlab.com/soccrates-eu/helm-chart/commit/bc5f954fb320fcdd186fd80e93e36fe53d54ffc8))

## [0.1.26](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.25...0.1.26) (2022-06-27)


### Bug Fixes

* removes portainer from chart, as it should be installed separately ([766bf58](https://gitlab.com/soccrates-eu/helm-chart/commit/766bf5891c3ec4d1b5ef4c662bad2e2f529dae0d))

## [0.1.25](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.24...0.1.25) (2022-06-27)


### Bug Fixes

* namespace traefik by default ([9a4fdef](https://gitlab.com/soccrates-eu/helm-chart/commit/9a4fdefcd842eac527ccea7b1b14bd5089018da2))

## [0.1.24](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.23...0.1.24) (2022-06-27)


### Bug Fixes

* moves authelia ingress to ingressRoute.yaml ([3b356be](https://gitlab.com/soccrates-eu/helm-chart/commit/3b356be8c7ff0eb97ccb72f4aefe1a40c3f1a6df))

## [0.1.23](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.22...0.1.23) (2022-06-25)


### Bug Fixes

* portainer port ([871dc38](https://gitlab.com/soccrates-eu/helm-chart/commit/871dc381847520bfbeef2007a3216b291f38632b))

## [0.1.22](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.21...0.1.22) (2022-06-25)


### Bug Fixes

* portainer port ([98e8f15](https://gitlab.com/soccrates-eu/helm-chart/commit/98e8f1532f445cc71300ed4d3146998461fe645e))

## [0.1.21](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.20...0.1.21) (2022-06-25)


### Bug Fixes

* moves portainer ingress to ingressRoute.yaml ([6502c06](https://gitlab.com/soccrates-eu/helm-chart/commit/6502c069b54ea45e73e23aa1161df02b0ab8e72c))

## [0.1.20](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.19...0.1.20) (2022-06-25)


### Bug Fixes

* moves private ingress to ingressRoute ([d30b564](https://gitlab.com/soccrates-eu/helm-chart/commit/d30b5642d8ec80f3eaffb45744066c9d3c7d9311))

## [0.1.19](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.18...0.1.19) (2022-06-25)


### Bug Fixes

* removes subdomain from ingressroute ([895061c](https://gitlab.com/soccrates-eu/helm-chart/commit/895061cadcce2d17fd8d2c80aa527bdcf5243652))

## [0.1.18](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.17...0.1.18) (2022-06-25)


### Bug Fixes

* uses traefik ingressRoute for public ([72b5282](https://gitlab.com/soccrates-eu/helm-chart/commit/72b5282849431ec4ea93320175161eb11d3a6ced))

## [0.1.17](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.16...0.1.17) (2022-06-25)


### Bug Fixes

* redis deployment name ([03c5e0b](https://gitlab.com/soccrates-eu/helm-chart/commit/03c5e0b3f4254a64ce0aef29a59ad3a1c3d8c98e))
* removes traefik crd ([bc517ef](https://gitlab.com/soccrates-eu/helm-chart/commit/bc517ef8fb9c2aeccf63e0f40ecfc3dd240c90fd))

## [0.1.16](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.15...0.1.16) (2022-06-25)


### Bug Fixes

* adds missing subdomains in values.yaml ([005e4b0](https://gitlab.com/soccrates-eu/helm-chart/commit/005e4b015db11449aa47f063c662f7946778475c))

## [0.1.15](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.14...0.1.15) (2022-06-25)


### Bug Fixes

* adds custom ingresses here ([280921e](https://gitlab.com/soccrates-eu/helm-chart/commit/280921e9e4c19dbff957f130c94a3e58c82a7683))
* ingress.public.yaml ([4f8a2e3](https://gitlab.com/soccrates-eu/helm-chart/commit/4f8a2e357a8d9037f1c64555617941318d0b765f))

## [0.1.14](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.13...0.1.14) (2022-06-25)


### Bug Fixes

* closes [#5](https://gitlab.com/soccrates-eu/helm-chart/issues/5) ([2b7b16f](https://gitlab.com/soccrates-eu/helm-chart/commit/2b7b16fdf05017a2d5f729f707578f0108a1f715))

## [0.1.13](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.12...0.1.13) (2022-06-25)


### Bug Fixes

* restores defaults ([e362e5e](https://gitlab.com/soccrates-eu/helm-chart/commit/e362e5e91251ce098a29da27df0de5a66bde8e7d))

## [0.1.12](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.11...0.1.12) (2022-06-25)


### Bug Fixes

* namespace traefik rbac ClusterRole by default ([45d6475](https://gitlab.com/soccrates-eu/helm-chart/commit/45d6475d6e9bec0f62ff4b79374d638656f6c395))

## [0.1.11](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.10...0.1.11) (2022-06-25)


### Bug Fixes

* namespace in forwardauth middleware ([bac196f](https://gitlab.com/soccrates-eu/helm-chart/commit/bac196ffe08031d6f5474f59de3c2a70e22aaaab))
* updates whoami to 0.1.5, which allows to set namespace ([c91e7ea](https://gitlab.com/soccrates-eu/helm-chart/commit/c91e7ea90d6929396c3c96a8511f22bd74040630))

## [0.1.10](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.9...0.1.10) (2022-06-24)


### Bug Fixes

* restores private and public with subchart whoami ([db85cd2](https://gitlab.com/soccrates-eu/helm-chart/commit/db85cd21647e68f50265c880e11cc7dd171f432f))

## [0.1.9](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.8...0.1.9) (2022-06-24)


### Bug Fixes

* makes domain global ([4a89f66](https://gitlab.com/soccrates-eu/helm-chart/commit/4a89f66fb6da61acae751d4584c2a7ae725a7638))

## [0.1.8](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.7...0.1.8) (2022-06-24)


### Bug Fixes

* domain should not be global ([f5561ba](https://gitlab.com/soccrates-eu/helm-chart/commit/f5561ba9165930dabf921de12bdfe77f36deb45f))

## [0.1.7](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.6...0.1.7) (2022-06-24)


### Bug Fixes

* adds whoami to charts ([620c81a](https://gitlab.com/soccrates-eu/helm-chart/commit/620c81a59231b3d80fd26b9cd428c0aa0ad148aa))

## [0.1.6](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.5...0.1.6) (2022-06-24)


### Bug Fixes

* removes private ([8fdd471](https://gitlab.com/soccrates-eu/helm-chart/commit/8fdd47142001aead6bea7cd4d407dacaad13b20c))

## [0.1.5](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.4...0.1.5) (2022-06-23)


### Bug Fixes

* generalise just the domain ([626cbcc](https://gitlab.com/soccrates-eu/helm-chart/commit/626cbcc1a18c584ef7650446231b6d715d34eaef))

## [0.1.4](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.3...0.1.4) (2022-06-23)


### Bug Fixes

* restores values.yaml to 1.0 (working) ([cdbeafe](https://gitlab.com/soccrates-eu/helm-chart/commit/cdbeafe0b3e4db7403e942f2705052f270322a54))

## [0.1.3](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.2...0.1.3) (2022-06-23)


### Bug Fixes

* make authelia and portainer optional dependencies ([0607450](https://gitlab.com/soccrates-eu/helm-chart/commit/06074506e20c31f061b26cb5ba7fc4a7cf41ab27))
* unique keys ([a4499a1](https://gitlab.com/soccrates-eu/helm-chart/commit/a4499a17205c452bfa5cf60d06dc2d18ccbf4755))

## [0.1.2](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.1...0.1.2) (2022-06-23)


### Bug Fixes

* generalises default values.yaml ([f5e6ee3](https://gitlab.com/soccrates-eu/helm-chart/commit/f5e6ee3653ef3bf59f0ed5bf99f5026d0ba9e608))

## [0.1.1](https://gitlab.com/soccrates-eu/helm-chart/compare/0.1.0...0.1.1) (2022-06-23)


### Bug Fixes

* removes default authelia values ([4df7698](https://gitlab.com/soccrates-eu/helm-chart/commit/4df76985e9e54adee585c9cca4beaa92f246651c))
